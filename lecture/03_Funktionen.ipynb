{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "# Funktionen\n",
    "\n",
    "Die meisten nicht-trivialen Programme haben Code-Teile, die mehrfach genutzt werden sollen. Es bietet sich an, diese in selbst-definierte Funktionen auszulagern. Ausserdem erhöhen Funktionen mit klar definiertem Verhalten, Argumenten und Rückgabewerten die Verständlichkeit des Programms.\n",
    "\n",
    "Funktionen werden mit dem zusammengesetzten Befehl `def` definiert:\n",
    "\n",
    "``` python\n",
    "def <Funktionsname>(<Argument1>, <Argument2>, ...):\n",
    "    <Befehl1>\n",
    "    <Befehl2>\n",
    "    ...\n",
    "```\n",
    "\n",
    "- Der Name ist ein Bezeichner, über den die Funktion aufgerufen werden kann. Die Argument-Deklarationen sind ebenfalls Bezeichner, über die innerhalb der Funktion auf die übergebenen Argumente zugegriffen werden kann.\n",
    "- Nach dem Doppelpunkt folgt ein Code-Block. Jede Zeile des Blocks **muss** in gleicher Weise eingerückt sein -- Python entscheidet durch die Einrückung, welche Befehle zum Block gehören.\n",
    "- Üblich ist eine Einrückung von vier Leerzeichen.\n",
    "\n",
    "Bei jedem Aufruf der Funktion werden die Befehle des Code-Blocks der Reihe nach abgearbeitet. Innerhalb der Funktion kann der Befehl\n",
    "\n",
    "```python\n",
    "return <Ausdruck>\n",
    "```\n",
    "\n",
    "verwendet werden, um die Funktion zu beenden und den Wert des Ausdrucks an den Aufrufer zurückzuliefern."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "x = 2\n",
      "y = 3\n",
      "Summe = 5\n"
     ]
    }
   ],
   "source": [
    "def print_and_add(x, y):\n",
    "    print(\"x = {0}\".format(x))\n",
    "    print(\"y = {0}\".format(y))\n",
    "    return x+y\n",
    "    print(\"Dieser Teil wird nicht mehr ausgeführt.\")\n",
    "\n",
    "result = print_and_add(2, 3)\n",
    "print(\"Summe = {}\".format(result))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "## Anonyme Funktionen\n",
    "\n",
    "Manchmal ist es nützlich, einfache Funktionen zur einmaligen Verwendung definieren zu können. In Python ist das (eingeschränkt) möglich mit\n",
    "\n",
    "```python\n",
    "lambda <Argument1>, <Argument2>, ...: <Ausdruck>\n",
    "```\n",
    "\n",
    "Der Funktions-Körper ist dabei nur ein einzelner Ausdruck, keine Befehls-Liste. Der Wert des Ausdrucks ist der Rückgabewert; `return` ist unötig."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "12"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def apply_twice(f, x):\n",
    "    return f(f(x))\n",
    "\n",
    "apply_twice(lambda x: 2*x, 3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "## Optionale Argumente\n",
    "\n",
    "Funktionen können mit optionalen Argumenten deklariert werden:\n",
    "\n",
    "```python\n",
    "def <Funktionsname>(<Argument1>, ..., <Optional1>=<Default1>, ...):\n",
    "    <Befehl1>\n",
    "    <Befehl2>\n",
    "    ...\n",
    "```\n",
    "\n",
    "Die optionalen Argumente müssen beim Aufruf nicht angegeben werden. Für jedes ausgelassenen Argument wird der entsprechende Default-Wert verwendet.\n",
    "\n",
    "Optionale Argumente stehen immer hinter den notwendigen Argumenten.\n",
    "\n",
    "*Bemerkung*: auch *benannte Argumente* (named arguments) genannt."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "x = 2\n",
      "y = 3\n",
      "5\n",
      "x = 2\n",
      "y = 2\n",
      "4\n",
      "x = 1\n",
      "y = 2\n",
      "3\n"
     ]
    }
   ],
   "source": [
    "def print_and_add(x=1, y=2):\n",
    "    print(\"x = {0}\".format(x))\n",
    "    print(\"y = {0}\".format(y))\n",
    "    return x+y\n",
    "\n",
    "print(print_and_add(2, 3))\n",
    "print(print_and_add(2))\n",
    "print(print_and_add())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "Ausserdem kann der Name der optionalen Argumente beim Aufruf explizit angegeben werden. Das erhöht die Lesbarkeit und ist nützlich, um z.B. nur das erste optionale Argument auszulassen, das zweite aber anzugeben.\n",
    "\n",
    "*Bemerkung*: Das wird dann sehr ausgiebig bei komplexeren Funktionen genutzt."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "x = 1\n",
      "y = 5\n",
      "6\n"
     ]
    }
   ],
   "source": [
    "print(print_and_add(y=5))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.6"
  },
  "name": "03_Funktionen.ipynb"
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
