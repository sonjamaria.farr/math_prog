# -*- coding: utf-8 -*-
"""
@author: jschulz1
"""

import numpy as np
import scipy as sp
from multiprocessing import Pool
#import sharedmem
import matplotlib.pyplot as py
import time
from numpy import mgrid, zeros
from numba import jit

from multiprocessing import cpu_count
default_nprocs = cpu_count()
def distribute(nitems, nprocs=None):
    if nprocs is None:
        nprocs = default_nprocs
    nitems_per_proc = int((nitems+nprocs-1)/nprocs)
    return [(i, min(nitems, i+nitems_per_proc)) for i in range(0, nitems, nitems_per_proc)]


@jit
def mandelbox (cent, xsize, ysize):
    si = 500
    [X,Y] = np.mgrid[cent[0]-xsize/2.:cent[0]+xsize/2.:si*1j,cent[1]-ysize/2.:cent[1]+ysize/2.:si*1j]
    C = (X + 1j*Y)
    Z = np.copy(C)

    it_max = 500
    Anz = np.zeros(Z.shape)

    for k in range(1,it_max+1):
        Z = Z**2+C
        #for i in range(0,si):
        #    for j in range(0,si):
        #        Z[i,j] = Z[i,j]**2 + C[i,j]
        Anz += np.isfinite(Z)
    return Anz,X,Y

def mandelone(cent, xsize, ysize, si):
    [X,Y] = np.mgrid[cent[0]-xsize/2.:cent[0]+xsize/2.:si*1j,cent[1]-ysize/2.:cent[1]+ysize/2.:si*1j]
    C = (X + 1j*Y)
    Anz = np.zeros((si,si))
  
    @jit(nopython=True)
    def mo(Anz,C,si):
      for a in range(0,si):
          for b in range(0,si):
              Anz[a,b] = mandel(C[a,b])
    mo(Anz, C, si)
    return Anz, X,Y

def mandelonep(C, imin, imax):
    si = np.shape(C)
    res = np.zeros((imax-imin,))
    for idx in range(imin,imax):
        res[idx-imin] = mandel(C[np.unravel_index(idx,si)])
    return res



@jit
def mandel(z):
    c = z
    maxiter = 500
    for n in range(0,maxiter):
        if abs(z) > 2:
            return n-1
        z = z**2 + c
    return maxiter

xsize = 2.0
ysize = 2.0
cent = [0., 0.]
si = 2000


[X,Y] = np.mgrid[cent[0]-xsize/2.:cent[0]+xsize/2.:si*1j,cent[1]-ysize/2.:cent[1]+ysize/2.:si*1j]
#result array
Anz = np.zeros((si,si))

#tic= time.time()
#Anz,X,Y = mandelbox (cent, xsize, ysize)
#print(time.time()-tic)

if True:
    C = (X + 1j*Y)

    tic= time.time()
    Anz,X,Y = mandelone(cent, xsize, ysize,  si)
    print(time.time()-tic)

if False:
    #create shared Array of complex plane numbers
    #C = sharedmem.empty((si,si), np.complex)
    C[:] =(X + 1j*Y)
    # make multiprocess Pool of workers
    p = Pool(2)

    #distribute slices according the number of processes
    slices = distribute(int(si*si),2)

    # make parallel over all lines
    tic= time.time()
    results = [p.apply_async(mandelonep, (C, imin, imax)) for (imin, imax) in slices]
    for r, (imin, imax) in zip(results, slices):
        Anz[np.unravel_index(range(imin,imax),(si,si))] = r.get()
    print(time.time()-tic)


if False:
    py.imshow(Anz)
    py.show()
