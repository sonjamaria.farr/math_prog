#!/usr/bin/env python3

import argparse
import json
import sys

p = argparse.ArgumentParser()
p.add_argument('--leave-metadata', action='store_true')
p.add_argument('--leave-exec-count', action='store_true')
p.add_argument('--clean-outputs', action='store_true')
p.add_argument('--clean-code-cells', action='store_true')
p.add_argument('--add-empty', action='store_true')
args = p.parse_args()

empty = {
    'cell_type': 'code',
    'execution_count': 1,
    'metadata': {
        'collapsed': False,
        'editable': True,
        'deletable': True
    },
    'source': [],
    'outputs': []
}

js = json.load(sys.stdin)

cells = []
for cell in js['cells']:
    if not args.leave_metadata:
        cell['metadata'] = empty['metadata']

    if cell['cell_type'] != 'code':
        cells.append(cell)
        if args.add_empty:
            cells.append(empty)
        continue

    if args.clean_code_cells:
        continue

    if not args.leave_exec_count:
        cell['execution_count'] = empty['execution_count']
    if args.clean_outputs:
        cell['outputs'] = empty['outputs']

    cells.append(cell)

js['cells'] = cells

print(json.dumps(js, sort_keys=True, indent=1, ensure_ascii=False))
