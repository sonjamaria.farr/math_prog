#!/usr/bin/env bash

cd "$(git rev-parse --show-toplevel)"

git config include.path ../.gitconfig
# ln -sfr scripts/pre-commit.sh .git/hooks/pre-commit
