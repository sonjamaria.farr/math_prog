#!/usr/bin/env bash

exec 2>&1

cd "$(git rev-parse --show-toplevel)"

stashed=false

stash() {
    if ! $stashed; then
       git stash -q --keep-index
       stashed=true
    fi
}

stash-pop() {
    $stashed && git stash pop -q
}

trap stash-pop EXIT

if git diff --cached --name-only | grep -q '^exercises/'; then
    stash
    scripts/make-exercises.sh || exit 1
    git add exercises/*.ipynb
fi

if git diff --cached --name-only | grep -q '^lecture/'; then
    stash
    # TODO: Ignore errors here for now, in case plumbum is not available.
    scripts/make-nb-links.py
    git add README.md
fi

exit 0
