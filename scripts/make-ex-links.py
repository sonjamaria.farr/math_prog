#!/usr/bin/env python3

from plumbum import local
from plumbum.cmd import git

local.cwd.chdir(git('rev-parse', '--show-toplevel').strip())

readme = local.cwd / 'README.md'
tmp = local.cwd / 'README.md.tmp'

# jump to correct place and save stuff up to start of list
with readme.open('r') as f, tmp.open('w') as fout:
    flag = True
    for line in f:
        if flag:
            fout.write(line)

        if not flag and line.find("## ") != -1:
            flag = True
            fout.write(line)

        if flag and line.find("## Übungsaufgaben") != -1:
            flag = False
            fout.write("\n")
            for nb in sorted(local.path("exercises/") // "*.ipynb"):
                title = nb.basename.replace(".ipynb", "")
                link = "* [{}](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/exercises/{}.ipynb)\n".format(title, title)
                print(link)
                fout.write(link)
            fout.write("\n")

tmp.rename(readme)
