# Mathematisch orientiertes Programmieren

Dieser Kurs richtet sich an Studenten der Naturwissenschaften, insbesondere
an Mathematiker im Mono - Bachelor (als verpflichtender Programmierkurs),
in den ersten Semestern.

Es soll eine fundierte Basis-Ausbildung im Programmieren im naturwissenschaftlich/mathematischen
Umfeld stattfinden. Die gewählte Programmiersprache ist hier Python.

Folgende Themen werden vermittelt:

- Grundprinzipien der Programmierung (anhand von Python)
- elementare Algorithmen und deren Anwendungen
- Kontroll- und Datenstrukturen
- Grundzüge des imperativen, objektorientierten und funktionalen Programmierens
- Bibliotheken zur Lösung naturwissenschaftlicher Fragestellungen einsetzen
- verschiedene Methoden der Visualisierung
- Verschiedene Möglichkeiten der Performance-Verbesserung (u.A. Parallelisierung)
- Grundtechniken der Projektverwaltung (Versionskontrolle, Arbeiten im Team)

## Vorlesungs-notebooks

* [01_Linux und Terminals](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/lecture/01_Linux und Terminals.ipynb)
* [02_Allgemeines](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/lecture/02_Allgemeines.ipynb)
* [03_Funktionen](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/lecture/03_Funktionen.ipynb)
* [04_Kontrollstrukturen](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/lecture/04_Kontrollstrukturen.ipynb)
* [05_Module und Editoren](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/lecture/05_Module und Editoren.ipynb)
* [06_Funktionen 2](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/lecture/06_Funktionen 2.ipynb)
* [07_Funktionales Programmieren](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/lecture/07_Funktionales Programmieren.ipynb)
* [08_Input Output](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/lecture/08_Input Output.ipynb)
* [09_Numpy](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/lecture/09_Numpy.ipynb)
* [10_Lineare Algebra](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/lecture/10_Lineare Algebra.ipynb)
* [11 Grafiken 2D](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/lecture/11 Grafiken 2D.ipynb)
* [12_Numpy, holoviews IO](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/lecture/12_Numpy, holoviews IO.ipynb)
* [13 Grafiken 3D](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/lecture/13 Grafiken 3D.ipynb)
* [14_Andere Visualisierungen](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/lecture/14_Andere Visualisierungen.ipynb)
* [15_Symbolisches Rechnen](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/lecture/15_Symbolisches Rechnen.ipynb)
* [16_Integration](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/lecture/16_Integration.ipynb)
* [17_Interpolation](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/lecture/17_Interpolation.ipynb)
* [18_Nichtlineare Gleichungen](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/lecture/18_Nichtlineare Gleichungen.ipynb)
* [19_Objektorientierung](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/lecture/19_Objektorientierung.ipynb)
* [20_Exceptions](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/lecture/20_Exceptions.ipynb)
* [22_performance](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/lecture/22_performance.ipynb)
* [23_Parallelisierung](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/lecture/23_Parallelisierung.ipynb)
* [24_git](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/lecture/24_git.ipynb)
* [25_Dokumentengenerator](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/lecture/25_Dokumentengenerator.ipynb)
* [26_Showcases](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/lecture/26_Showcases.ipynb)

## Übungsaufgaben

* [01_Linux und Terminals_exercise](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/exercises/01_Linux und Terminals_exercise.ipynb)
* [02_allgemeines_exercise](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/exercises/02_allgemeines_exercise.ipynb)
* [03_funktionen_exercise](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/exercises/03_funktionen_exercise.ipynb)
* [04_Kontrollsktruturen_exercise](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/exercises/04_Kontrollsktruturen_exercise.ipynb)
* [05_Module_exercise](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/exercises/05_Module_exercise.ipynb)
* [06_Funktionen_2_exercise](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/exercises/06_Funktionen_2_exercise.ipynb)
* [07_Funktionales Programmieren_exercise](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/exercises/07_Funktionales Programmieren_exercise.ipynb)
* [08_Input Output_exercise](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/exercises/08_Input Output_exercise.ipynb)
* [09_Numpy_exercise](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/exercises/09_Numpy_exercise.ipynb)
* [10_Lineare Algebra_exercise](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/exercises/10_Lineare Algebra_exercise.ipynb)
* [11_Grafiken_exercise](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/exercises/11_Grafiken_exercise.ipynb)
* [12_Numpy, Matplotlib IO_exercise](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/exercises/12_Numpy, Matplotlib IO_exercise.ipynb)
* [12_Numpy, holoviews IO_exercise](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/exercises/12_Numpy, holoviews IO_exercise.ipynb)
* [13_Grafiken 3D_exercise](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/exercises/13_Grafiken 3D_exercise.ipynb)
* [14_Andere Visualisierungen_exercise](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/exercises/14_Andere Visualisierungen_exercise.ipynb)
* [15_Symbolisches Rechnen_exercise](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/exercises/15_Symbolisches Rechnen_exercise.ipynb)
* [16_Integration_exercise](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/exercises/16_Integration_exercise.ipynb)
* [17_Interpolation_exercise](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/exercises/17_Interpolation_exercise.ipynb)
* [18_Nichtlineare Gleichungen_exercise](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/exercises/18_Nichtlineare Gleichungen_exercise.ipynb)
* [19_Objektorientierung_exercise](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/exercises/19_Objektorientierung_exercise.ipynb)
* [20_Exceptions_exercise](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/exercises/20_Exceptions_exercise.ipynb)
* [21_GUI_exercise](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/exercises/21_GUI_exercise.ipynb)
* [22_Performance_exercise](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/exercises/22_Performance_exercise.ipynb)
* [23_Parallelisierung_exercise](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/exercises/23_Parallelisierung_exercise.ipynb)
* [25_Dokumentengenerator_exercise](http://nbviewer.jupyter.org/urls/gitlab.gwdg.de/jschulz1/math_prog/raw/master/exercises/25_Dokumentengenerator_exercise.ipynb)

## Links

* [scipy lectures](http://www.scipy-lectures.org/)

## Abhaengigkeiten (Module)

Mit bestehenden jupyter (z.B. jupyter.gwdg.de)

    numpy scipy matplotlib numba sympy holoviews bokeh networkx 

Auf dem jupyter server macht man das im terminal mit

    pip install --user numpy scipy matplotlib numba sympy holoviews networkx

eigene Installation

    numpy ipython scipy matplotlib numba line_profiler mayavi jupyter pillow sympy nose holoviews ipyparallel networkx sphinx
